<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="https://www.google.com/recaptcha/api.js"></script>

        <script>
            function onSubmit(token) {
              document.getElementById("sign-in").submit();
            }
          </script>
    </head>
    <body>
        <div id="non-spa-app">
            <main class="py-0">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </main>
        </div>
    </body>
</html>
