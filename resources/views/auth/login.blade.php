@extends('layouts.login')

@section('content')
    <div class="row">
        <div class=" d-none d-sm-block col-lg-7 col-md-5" style="height: 100vh;
        background: url(https://images.pexels.com/photos/796602/pexels-photo-796602.jpeg?cs=srgb&dl=three-white-ceramic-pots-with-green-leaf-plants-near-open-796602.jpg&fm=jpg);
        background-size: 160%; background-position: center;">
        </div>
        <div class="col-lg-5 col-md-7" style="height: 100vh;">
            <div class="container">
                <form method="POST" action="{{ route('login') }}" id="sign-in">
                    @csrf
                   
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col text-center">
                                <h1 class="font-weight-bold">{{ config('app.name', 'Laravel') }}</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="username">{{ __('E-Mail Address') }}</label>
                            </div>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="username">{{ __('Password') }}</label>
                            </div>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
        
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                            </div>
                            <div class="col-md-6">
                                <button
                                    class="g-recaptcha btn btn-secondary btn-block" 
                                    data-sitekey="6Ldl9LcZAAAAAPj2nmknMwILTlfcA2VgrkFfH5Nu" 
                                    data-callback='onSubmit' 
                                    data-action='submit'
                                    type="submit">
                                    Sign In
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection