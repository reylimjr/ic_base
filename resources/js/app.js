/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import router from "./router";
import App from "./components/App";
import Vuetify from 'vuetify';
import "vuetify/dist/vuetify.min.css";
import '@mdi/font/css/materialdesignicons.css'

window.Vue = require('vue');
require('./bootstrap');


Vue.use(Vuetify);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);



Vue.component('side-nav', require('./components/MenuComponent.vue').default);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({
        theme: {
            dark: false,
            themes: {
                light: {
                    primary: "#607d8b",
                    secondary: "#795548",
                    accent: "#009688",
                    error: "#f44336",
                    warning: "#ff5722",
                    info: "#2196f3",
                    success: "#4caf50"
                },
                dark: {
                    primary: "#607d8b",
                    secondary: "#795548",
                    accent: "#009688",
                    error: "#f44336",
                    warning: "#ff5722",
                    info: "#2196f3",
                    success: "#4caf50"
                },
            },
        },
    }),
    router, // <-- register router with Vue
    render: (h) => h(App), // <-- render App component
    data: () => ({
        drawer: null
    }),
});