import Vue from "vue";
import VueRouter from "vue-router";

import AdminHome from "./components/Admin/Home";
import PassportIndex from "./components/Admin/Passport";
import Default from "./components/Default";
import AdminAccountTable from "./components/Admin/Users/Table";
import AdminAccountNew from "./components/Admin/Users/New";
import AdminAccountEdit from "./components/Admin/Users/Edit";
import AdminRoles from "./components/Admin/Roles/Table";
import AdminRolesNew from "./components/Admin/Roles/New";
import AdminRolesEdit from "./components/Admin/Roles/Edit";
import AdminPermission from "./components/Admin/Permission/Table";
import AdminPermissionNew from "./components/Admin/Permission/New";
import AdminPermissionEdit from "./components/Admin/Permission/Edit";
import AccountProfile from "./components/Account/Profile";
import AdminBaseUserDetails from "./components/Admin/Base/UserDetails";

Vue.use(VueRouter);
const router = new VueRouter({
    mode: "history",
    routes: [{
            path: "/",
            name: 'admin.home',
            component: AdminHome
        },

        {
            path: "/admin/passport",
            name: 'admin.passport',
            component: PassportIndex
        },
        {
            path: "/admin/users",
            component: Default,
            children: [
                { name: 'admin.users', path: '', component: AdminAccountTable },
                { name: 'admin.users.new', path: 'new', component: AdminAccountNew },
                { name: 'admin.users.edit', path: 'edit/:id', component: AdminAccountEdit },
            ]
        },
        {
            path: "/admin/user/details",
            component: Default,
            children: [
                { name: 'admin.user.details', path: '', component: AdminBaseUserDetails },
            ]
        },
        {
            path: "/admin/roles",
            component: Default,
            children: [
                { name: 'admin.roles', path: '', component: AdminRoles },
                { name: 'admin.roles.new', path: 'new', component: AdminRolesNew },
                { name: 'admin.roles.edit', path: 'edit/:id', component: AdminRolesEdit },
            ]
        },
        {
            path: "/admin/permission",
            component: Default,
            children: [
                { name: 'admin.permission', path: '', component: AdminPermission },
                { name: 'admin.permission.new', path: 'new', component: AdminPermissionNew },
                { name: 'admin.permission.edit', path: 'edit/:id', component: AdminPermissionEdit },
            ]
        },
        {
            path: "/account/profile",
            component: Default,
            children: [
                { name: 'account.profile', path: '', component: AccountProfile },
            ]
        }

    ]
});

export default router;