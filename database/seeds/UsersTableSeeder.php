<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = config('roles.models.role')::where('name', '=', 'User')->first();
        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        $permissions = config('roles.models.permission')::all();

        $users = ['John','Jane', 'Demo'];
        /*
         * Add Users
         *
         */

        if (config('roles.models.defaultUser')::where('email', '=', 'user@user.com')->first() === null) {

            foreach($users as $u)
            {
                $newUser = config('roles.models.defaultUser')::create([
                    'first_name'     => $u,
                    'last_name'     => $u,
                    'email'    => $u.'@user.com',
                    'password' => bcrypt('password'),
                ]);
                $newUser->attachRole($userRole);
            }
           
        }
    }
}
