<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
// use Database\Seeds\createAdminUserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Model::unguard();

        $this->call(createAdminUserSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserDetailsSeeder::class);

        Model::reguard();
    }
}
