<?php

use App\BaseUserDetails;
use Illuminate\Database\Seeder;

class UserDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseUserDetails::create([
            'title' => 'Address',
            'name' => 'address',
            'type' => 'text-field',
            'status' => 'Enabled',
            'position_order' => 1,
            'is_required' => 1,
        ]);

    }
}
