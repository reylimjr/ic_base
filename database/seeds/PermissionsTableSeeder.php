<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */

        $permission_array = ['Users', 'User Details'];
        $types = ['View', 'Create', 'Edit', 'Delete'];
        $Permissionitems = [];
        $p_array = [];

        foreach ($permission_array as $v) {
            $t = [];

            foreach ($types as $type) {
                $v_lower = str_replace(' ', '.', \strtolower($v));
                $t_lower = strtolower($type);
                $t[] = [
                    'name' => "Can $type $v",
                    'slug' => "{$t_lower}.{$v_lower}",
                    'description' => "Can $type $v",
                    'model' => 'Permission',
                ];
            }

            $Permissionitems = array_merge($Permissionitems, $t);
        }

        /*
         * Add Permission Items
         *
         */
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem['slug'])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::create([
                    'name' => $Permissionitem['name'],
                    'slug' => $Permissionitem['slug'],
                    'description' => $Permissionitem['description'],
                    'model' => $Permissionitem['model'],
                ]);
            }
        }
    }
}
