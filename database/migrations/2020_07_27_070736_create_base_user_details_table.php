<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("title");
            $table->string("type");
            $table->string("status");
            $table->integer("position_order");
            $table->boolean("is_required");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_user_details');
    }
}
