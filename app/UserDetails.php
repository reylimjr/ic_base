<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * UserDetails
 */
class UserDetails extends Model
{
    //

        
    /**
     * user_details
     *
     * @return void
     */
    public function user_details()
    {
        return $this->belongsTo('User');
    }
}
