<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function __construct()
    {

    }
    /* Start Roles */

    public function Roles_Index(Request $request)
    {
        if ($request->user()->isAdmin()) {
            $roles = config('roles.models.role')::get();

            return response()->json([
                'roles' => $roles,
            ]);
        } else {
            abort(401, 'Permission denied');
        }
    }

    public function Roles_Get($id)
    {
        if (Auth::user()->isAdmin()) {

            $role = config('roles.models.role')::findOrFail($id);

            return response()->json([
                'role' => $role,
                'permission' => $role->permissions()->get(),
            ]);
        }
    }

    public function Roles_Store(Request $request)
    {
        if (Auth::user()->isAdmin()) {

            $roles = config('roles.models.role');

            $request->validate([
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
                'level' => 'required',
            ]);

            $role = new $roles;

            $role->name = $request->name;
            $role->slug = $request->slug;
            $role->description = $request->description;
            $role->level = $request->level;
            $role->save();

            return response()->json([
                'role' => $role,
            ]);

        }
    }

    public function Roles_Update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {

            $role = config('roles.models.role')::find($id);

            $request->validate([
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
                'level' => 'required',
            ]);

            $role->name = $request->name;
            $role->slug = $request->slug;
            $role->description = $request->description;
            $role->level = $request->level;

            $permissions = collect($request->permissions);
            $role->detachAllPermissions();
            $role->syncPermissions($permissions->pluck('id'));

            $role->save();

            return response()->json([
                'role' => $role,
                'permission' => $role->permissions()->get(),
            ]);
        }
    }

    public function Roles_Delete($id)
    {
        if (Auth::user()->isAdmin()) {
            $role = config('roles.models.role')::find($id);
            $role->delete();

            return response()->json([
                'role' => $role,
            ]);
        }
    }
}
