<?php

namespace App\Http\Controllers;

use App\BaseUserDetails;
use Illuminate\Http\Request;

class BaseUserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->canViewUserDetails()) {
            $userDetails = BaseUserDetails::orderBy('position_order', 'ASC')->get();

            return response()->json([
                'status' => 'ok',
                'items' => $userDetails,
                'keys' => $userDetails->pluck('name'),
            ]);

        } else {
            abort(401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->canCreateUserDetails()) {
            $request->validate([
                'title' => 'required',
                'type' => 'required',
                'status' => 'required',
                'position_order' => ['required', 'integer'],
                'is_required' => 'required',
            ]);

            $details = new BaseUserDetails;
            $details->name = strtolower(str_replace(' ', '_', $request->title));
            $details->title = $request->title;
            $details->type = $request->type;
            $details->status = $request->status;
            $details->position_order = $request->position_order;
            $details->is_required = $request->is_required;
            $details->save();

            return response()->json([
                'status' => 'ok',
                'message' => 'user details saved.',
            ]);
        } else {
            abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BaseUserDetails  $baseUserDetails
     * @return \Illuminate\Http\Response
     */
    public function show(BaseUserDetails $baseUserDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BaseUserDetails  $baseUserDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(BaseUserDetails $baseUserDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BaseUserDetails  $baseUserDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->user()->canEditUserDetails()) {
            $request->validate([
                'title' => 'required',
                'type' => 'required',
                'status' => 'required',
                'position_order' => 'required',
                'is_required' => 'required',
            ]);

            $details = BaseUserDetails::find($request->id);
            $details->name = strtolower(str_replace(' ', '_', $request->title));
            $details->title = $request->title;
            $details->type = $request->type;
            $details->status = $request->status;
            $details->position_order = $request->position_order;
            $details->is_required = $request->is_required;
            $details->save();

            return response()->json([
                'status' => 'ok',
                'message' => "User details ({$request->title}) updated.",
            ]);
        } else {
            abort(401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BaseUserDetails  $baseUserDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->user()->canDeleteUserDetails()) {
            $details = BaseUserDetails::find($request->id);
            $details->delete();
            return response()->json([
                'status' => 'ok',
                'all' => $details,
                'message' => "User detail successfully deleted",
            ]);
        } else {
            abort(401);
        }
    }

    public function demo()
    {
        $permission_array = ['Users', 'User Details', 'Products'];
        $types = ['View', 'Create', 'Edit', 'Delete'];
        $Permissionitems = [];
        $p_array = [];

        foreach ($permission_array as $v) {
            $t = [];

            foreach ($types as $type) {
                $v_lower = str_replace(' ', '.', \strtolower($v));
                $t_lower = strtolower($type);
                $t[] = [
                    'name' => "Can $type $v",
                    'slug' => "{$t_lower}.{$v_lower}",
                    'description' => "Can $type $v",
                    'model' => 'Permission',
                ];
            }

            $Permissionitems = array_merge($Permissionitems, $t);
        }


        print_r($Permissionitems);
    }

}
