<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    //

    public function Permission_Index()
    {
        if (Auth::user()->isAdmin()) {
            $permissions = config('roles.models.permission')::get();

            return response()->json([
                'permissions' => $permissions,
            ]);
        }
    }

    public function Permission_Get($id)
    {
        if (Auth::user()->isAdmin()) {
            $permission = config('roles.models.permission')::findOrFail($id);

            return response()->json([
                'permission' => $permission,
            ]);
        }
    }

    public function Permission_Store(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $permissions = config('roles.models.permission');

            $request->validate([
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
                'model' => 'required',
            ]);

            $permission = new $permissions;

            $permission->name = $request->name;
            $permission->slug = $request->slug;
            $permission->description = $request->description;
            $permission->model = $request->model;
            $permission->save();

            return response()->json([
                'permission' => $permission,
            ]);
        }
    }

    public function Permission_Update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $permission = config('roles.models.permission')::find($id);

            $request->validate([
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
                'model' => 'required',
            ]);

            $permission->name = $request->name;
            $permission->slug = $request->slug;
            $permission->description = $request->description;
            $permission->model = $request->model;
            $permission->save();

            return response()->json([
                'permission' => $permission,
            ]);
        }
    }

    public function Permission_Delete($id)
    {
        if (Auth::user()->isAdmin()) {
            $permission = config('roles.models.permission')::find($id);
            $permission->delete();
            return response()->json([
                'permission' => $permission,
            ]);
        }
    }

}
