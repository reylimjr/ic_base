<?php

namespace App\Http\Controllers;

use App\BaseUserDetails;
use App\User;
use App\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->canViewUsers()) {
            $users = User::with(['UserDetails'])->where('id', '!=', 1)->get();
            return response()->json([
                'data' => $users,
            ]);
        } else {
            abort(401, 'Permission denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->canCreateUsers()) {

            // validate
            $validate = $this->validate_details($request);
            $validate = array_merge($validate, [
                'first_name' => 'required',
                'last_name' => 'required',
                'roles' => 'nullable|required',
                'email' => 'required|email|unique:users',
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $request->validate($validate);

            $user = new User;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);

            $user->save();

            /* Save user details */
            $this->user_details($request, $user);

            if ($request->roles) {
                //remove roles and add new
                $user->syncRoles($request->roles);
            }

            if ($request->roles) {
                //remove roles and add new
                $user->detachAllRoles();
                $user->syncRoles($request->roles);
            }

            return response()->json([
                'data' => $user,
            ]);

        } else {
            abort(401, 'Permission denied');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        if ($request->user()->canEditUsers()) {

            $u_details = BaseUserDetails::where('status', 'enabled')->get();
            $details = [];
            $user_details = $user->userDetails->keyby('key');

            foreach ($u_details as $v) {
                $details[$v->name] = (isset($user_details[$v->name])) ? $user_details[$v->name]->value : '';
            }
            $user->userDetails = $details;

            return response()->json([
                'user' => $user,
                'roles' => $user->roles()->pluck('roles.id'),
                'permissions' => $user->permissions()->get(),
            ]);
        } else {
            abort(401, 'Permission denied');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->user()->canEditUsers() || $request->user()->id == $request->id) {

            $validate = $this->validate_details($request);
            $validate = array_merge($validate, [
                'first_name' => 'required',
                'last_name' => 'required',
                'status' => 'required',
                'email' => 'required|email',
            ]);

            $request->validate($validate);

            if ($request->password) {
                $request->validate([
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                ]);
                $user->password = Hash::make($request->password);
            }

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->status = $request->status;
            $user->email = $request->email;

            /* Save user details */
            $this->user_details($request, $user);

            // remove all permissions and add new
            if ($request->permissions) {

                $permissions = collect($request->permissions);
                $user->detachAllPermissions();
                $user->syncPermissions($permissions->pluck('id'));
            }

            if ($request->roles) {
                //remove roles and add new
                $user->detachAllRoles();
                $user->syncRoles($request->roles);
            }

            $user->save();

            return response()->json([
                'user' => $user,
            ]);

        } else {
            abort(401, 'Permission denied');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Request $request)
    {
        if ($request->user()->canDeleteUsers() && $request->user()->id == 1) {
            UserDetails::where('user_id', $user->id)->delete();
            $user->delete();
            return response()->json([
                'user' => $user,
            ]);
        } else {
            abort(401, 'Permission denied');
        }
    }

    public function search(Request $request)
    {
        if ($request->search) {
            $users = User::where('first_name', 'LIKE', "%{$request->search}%")
                ->orWhere('last_name', 'LIKE', "%{$request->search}%")
                ->orWhere('email', 'LIKE', "%{$request->search}%")
                ->where('id', '!=', 1)
                ->get();
        } else {
            $users = User::with(['UserDetails'])->where('id', '!=', 1)->get();
        }

        return response()->json([
            'data' => $users,
        ]);

    }

    public function user_details($request, $user)
    {

        UserDetails::where('user_id', $user->id)->delete();
        $details = BaseUserDetails::where('status', 'enabled')->get();
        foreach ($details as $key => $value):
            $userDetails = new UserDetails();
            $userDetails->key = $value->name;
            $userDetails->value = ($request->userDetails[$value->name]) ? $request->userDetails[$value->name] : '';
            $userDetails->user_id = $user->id;
            $userDetails->save();
        endforeach;

    }

    public function validate_details($request)
    {
        $details = BaseUserDetails::where('is_required', 1)->where('status', 'enabled')->get();
        $validate = [];
        foreach ($details as $key => $value) {
            $validate = array_merge($validate, ['userDetails.' . $value->name => 'sometimes|required']);
        }

        return $validate;
    }

    public function get_profile(Request $request)
    {
        $user = User::with(['UserDetails'])->where('id', $request->user()->id)->first();
        $userdetails = $user->userDetails;

        $u_details = BaseUserDetails::where('status', 'enabled')->get();
        $details = [];
        $user_details = $user->userDetails->keyby('key');

        foreach ($u_details as $v) {
            $details[$v->name] = (isset($user_details[$v->name])) ? $user_details[$v->name]->value : '';
        }

        $user->userDetails = $details;

        return response()->json([
            'user' => $user,
            'details' => $u_details,
        ]);
    }
}
