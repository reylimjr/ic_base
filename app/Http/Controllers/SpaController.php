<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SpaController extends Controller
{
    public function index()
    {
        return view("spa");
    }

    public function admin()
    {
        return view("spa",  [
            'auth_user' => Auth::user()
        ]);
    }
}

