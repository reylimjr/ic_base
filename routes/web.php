<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::prefix('api/admin')->middleware(['ajax'])->group(function () {
    Route::Resource('user', 'UserController');
    Route::Resource('base/user/details', 'BaseUserDetailsController');

    Route::get('get/profile', 'UserController@get_profile')->name('get.profile');
    Route::get('get/demo', 'BaseUserDetailsController@demo')->name('get.demo');
    Route::post('search/user', 'UserController@search')->name('search.user');
    Route::delete('base/delete/user/details/{id}', 'BaseUserDetailsController@destroy')->name('destroy.userdetails');
    // Security Roles
    Route::get('roles', 'RoleController@Roles_Index')->name('roles.index');
    Route::get('roles/{id}', 'RoleController@Roles_Get')->name('roles.get');
    Route::put('roles/{id}', 'RoleController@Roles_Update')->name('roles.update');
    Route::delete('roles/{id}', 'RoleController@Roles_Delete')->name('roles.delete');
    Route::post('roles', 'RoleController@Roles_Store')->name('roles.store');
    
    // Security Permissions
    Route::get('permissions', 'PermissionController@Permission_Index')->name('permissions.index');
    Route::get('permissions/{id}', 'PermissionController@Permission_Get')->name('permissions.get');
    Route::put('permissions/{id}', 'PermissionController@Permission_Update')->name('permissions.update');
    Route::post('permissions', 'PermissionController@Permission_Store')->name('permissions.store');
    Route::delete('permissions/{id}', 'PermissionController@Permission_Delete')->name('permissions.delete');
    
});

Auth::routes();
Route::get('/auth/logout', 'Auth\LoginController@logout')->name('logout');


/* Vue routes */
Route::get('/admin/{vue}', 'SpaController@admin')->where('vue', '.*');
Route::get('/{vue}', 'SpaController@admin')->where('vue', '.*');
Route::get('/', 'SpaController@admin')->where('vue', '.*');
